#!/bin/bash

set -e

export LANG='en_US.UTF-8'

if [ "$1" = "rq_updater" ]; then
    command="cd source/rq && \
        rq worker --with-scheduler"
elif [ "$1" = "rabbit_updater" ]; then
    command="cd source/updater && \
        python3 updater.py"
elif [ "$1" = "rabbit_consumer" ]; then
    command="cd source/consumer && \
        python3 consumer.py"
else
    command="exec sh"
fi

eval "${command}"