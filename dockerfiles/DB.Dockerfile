FROM library/postgres:12
ENV PGDATA /var/lib/pgsql/data/
RUN apt-get -qq update && apt-get -qq -y --no-install-recommends install \
    postgresql-client-12 \
    postgresql-client-common \
    postgresql-common
