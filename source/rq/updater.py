import datetime
from redis import Redis
from rq import Queue

print("RQ Example Updater Started!")

queue = Queue(connection=Redis())


def rq_function():
    print("RQ Function started...")
    return datetime.datetime.now()


job = queue.enqueue_in(datetime.timedelta(seconds=10), rq_function)